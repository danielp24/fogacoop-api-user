package com.soaint.lineabase.commons.domains.request.accounts;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class RecoveredPassword {
    private String apiToken;
    private String userName;
    private String password;
}
