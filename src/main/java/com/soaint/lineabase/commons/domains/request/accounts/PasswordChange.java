package com.soaint.lineabase.commons.domains.request.accounts;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class PasswordChange {
    private String apiToken;
    private  String userName;
    private String oldPassword;
    private String newPassword;

}
