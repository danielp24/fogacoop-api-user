package com.soaint.lineabase.commons.constants.api.persona;

public interface EndpointPersonaApi {

    String PERSONA_API_V1 = "persona/v1";
    String FIND_PERSONAS = "/";
    String FIND_PERSONAS_BY_ID = "/{id}";
    String UPDATE_PERSONAS_BY_ID = "/{id}";
    String CREATE_PERSONA = "/";
}
