package com.soaint.lineabase.repository.persona;

import com.soaint.lineabase.model.entities.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface IPersonaRepository extends JpaRepository<Persona, String> {

    @Query("select p from Persona p where p.idPersona = :id")
    Optional<Persona> findPersonaByIdPersona(@Param("id") final String id);


}
