package com.soaint.lineabase.repository.persona.impl;

import com.soaint.lineabase.commons.domains.request.PersonaDTORequest;
import com.soaint.lineabase.model.entities.Persona;
import com.soaint.lineabase.repository.persona.IPersonaRepository;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Component
@Log4j
public class PersonaRepositoryImpl implements PersonaRepositoryFacade {

    private final IPersonaRepository repository;

    @Autowired
    public PersonaRepositoryImpl(IPersonaRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<Persona> registerPersona(PersonaDTORequest persona) {
        //TODO: piña hace los converters
        return Optional.of(repository.save(Persona.builder().idPersona(persona.getIdPersona()
                                            ).build()));
    }

    @Override
    public Optional<Persona> getPersonaById(String id) {
        Optional<Persona> persona = repository.findPersonaByIdPersona(id);
        return persona.isPresent() ? persona : Optional.of(new Persona());
    }

    @Override
    public Optional<Collection<Persona>> findPersonas() {
        Collection<Persona> personas = new ArrayList<>(repository.findAll());
        return Optional.of(personas);
    }
}
