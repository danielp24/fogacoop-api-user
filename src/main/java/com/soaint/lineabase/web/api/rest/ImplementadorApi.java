package com.soaint.lineabase.web.api.rest;

import com.soaint.lineabase.commons.domains.generic.PersonaDTO;
import com.soaint.lineabase.commons.domains.request.PersonaDTORequest;
import org.springframework.http.ResponseEntity;

public interface ImplementadorApi {

    ResponseEntity<?> findPersonas();

    ResponseEntity<?> findPersonById(final String id);

    ResponseEntity<?> updatePersonaById(final String id);

    ResponseEntity<?> createPersona(final PersonaDTORequest persona);

}
